#pragma once;

#include <vector>
#include <memory>
#include <unordered_map>
#include <SFML/Graphics.hpp>
#include "GameObject.h"

namespace BTFO
{

const int WindowWidth = 800;
const int WindowHeight = 600;
const int TileWidth = 40;
const int TileHeight = 10;

enum GameState
{
	Exiting,
	Running,
	Stopped,
	Uninitialized,

};

class Game
{
public:

	Game();
	~Game();

	void Start();
	void Stop();
	void Reset();

private:
	sf::RenderWindow _mainWindow;
	GameState _state;

	std::vector<std::shared_ptr<GameObject>> _targets;
	std::unordered_map<std::string, std::shared_ptr<GameObject>> _gameObjectMap;
	size_t _points;
	size_t _lives;

	sf::Font _arial;

	void SetupWindow();
	void GameLoop();
	void HandleStrafing();
	void CheckForCollisions();
	void DisplayScore();

};
}