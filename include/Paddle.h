#pragma once

#include <SFML/Graphics.hpp>
#include "GameObject.h"

namespace BTFO
{

class Paddle : public sf::RectangleShape, public GameObject
{
public:
	Paddle(const sf::Vector2f& vel = sf::Vector2f(0.0f, 0.0f));
	Paddle(const sf::Vector2f& size, const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(0.0f, 0.0f));

	virtual void UpdatePosition(const float& elapsedTime, const sf::RenderWindow& target);

	~Paddle();

private:
	//sf::Vector2f _velocity;

};
}