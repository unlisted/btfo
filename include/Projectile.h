#pragma once

#include <SFML/Graphics.hpp>
#include "GameObject.h"

namespace BTFO
{

class Projectile : public sf::CircleShape, public GameObject
{
public:
	Projectile(const sf::Vector2f& vel = sf::Vector2f(2.0f, 2.0f));
	Projectile(float rad, const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(2.0f, 2.0f));

	virtual void UpdatePosition(const float& elapsedTime, const sf::RenderWindow& target);

	~Projectile();

private:
	//sf::Vector2f _velocity;

};
}
