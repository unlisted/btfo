#pragma once

#include <SFML/Graphics.hpp>
#include "GameObject.h"

namespace BTFO
{

class Target : public sf::RectangleShape, public GameObject
{
public:
	Target(const sf::Vector2f& vel = sf::Vector2f(0.0f, 0.0f));
	Target(const sf::Vector2f& size, const sf::Vector2f& pos, const sf::Vector2f& vel = sf::Vector2f(0.0f, 0.0f));

	virtual void UpdatePosition(const float& elapsedTime, const sf::RenderWindow& target);

	~Target();


};
}