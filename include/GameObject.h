#pragma once

#include <SFML/Graphics.hpp>

namespace BTFO
{

class GameObject
{
public:
	GameObject(const sf::Vector2f& vel = sf::Vector2f(0.0f, 0.0f));
	virtual ~GameObject();

	virtual void Draw(sf::RenderWindow& target);

	virtual void UpdatePosition(const float& elapsedTime, const sf::RenderWindow& target) = 0;
	virtual bool CheckForCollision(const GameObject& target);// = 0;

	virtual const sf::Vector2f& Velocity() { return _velocity; }
	virtual void Velocity(const sf::Vector2f& vel) { _velocity = vel; }
	virtual const bool Visible() { return _visible; }
	virtual void Visible(const bool& v) { _visible = v; }

protected:
	sf::Vector2f _velocity;
	bool _visible;

};
}