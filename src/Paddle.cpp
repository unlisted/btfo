
#include <iostream>
#include "Paddle.h"

using namespace sf;
using namespace std;


namespace BTFO
{

Paddle::Paddle(const sf::Vector2f& vel):
	GameObject(vel)
{
}

Paddle::Paddle(const sf::Vector2f& size, const sf::Vector2f& pos, const sf::Vector2f& vel):
	RectangleShape(size),
	GameObject(vel)
{
	setPosition(pos);
	setFillColor(sf::Color::Blue);
}

Paddle::~Paddle()
{
}

void Paddle::UpdatePosition(const float& elapsedTime, const sf::RenderWindow& target)
{
	Vector2f moveBy = elapsedTime * _velocity;
	move(moveBy);

	const Vector2f size = getSize();
	const Vector2u windowSize = target.getSize();

	if (getPosition().x >= windowSize.x - size.x)
	{
		_velocity.x *= 0.0f;
		setPosition(windowSize.x - size.x, getPosition().y);
	}

	if (getPosition().x <= 0)
	{
		_velocity.x *= 0.0f;
		setPosition(0.0f, getPosition().y);
	}
}

}