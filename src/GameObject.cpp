#include <iostream>
#include "GameObject.h"

using namespace std;
using namespace sf;

namespace BTFO
{

GameObject::GameObject(const sf::Vector2f& vel):
	_velocity(vel),
	_visible(true)
{
}

GameObject::~GameObject()
{
}

void GameObject::Draw(sf::RenderWindow& target)
{
	try
	{
		if (_visible)
			target.draw(dynamic_cast<Drawable&>(*this));
	} catch (const bad_cast& e)
	{
		cerr << e.what() << '\n';
		cerr << "This object is not of type Drawable\n";
	}
}

bool GameObject::CheckForCollision(const GameObject& target)
{
	try
	{
		auto targetBounds = dynamic_cast<const Shape&>(target).getGlobalBounds();
		auto sourceBounds = dynamic_cast<const Shape&>(*this).getGlobalBounds();

		if (targetBounds.intersects(sourceBounds))
			return true;

	} catch (const bad_cast& e)
	{
		cerr << e.what() << '\n';
		cerr << "This object is not of type Shape\n";
		return false;
	}

	return false;
}

}