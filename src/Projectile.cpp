#include "Projectile.h"

using namespace sf;


namespace BTFO
{

Projectile::Projectile(const Vector2f& vel):
	GameObject(vel)
{
}

Projectile::Projectile(float rad, const Vector2f& pos, const Vector2f& vel):
	CircleShape(rad),
	GameObject(vel)
{
	setPosition(pos);
	setFillColor(sf::Color::Blue);
}

Projectile::~Projectile()
{
}

void Projectile::UpdatePosition(const float& elapsedTime, const sf::RenderWindow& target)
{
	Vector2f moveBy = elapsedTime * _velocity;
	move(moveBy);

	const float rad = getRadius();
	const Vector2u windowSize = target.getSize();


	if (getPosition().x >= windowSize.x - rad)
	{
		_velocity.x *= -1;
		setPosition(windowSize.x - rad, getPosition().y);
	}

	if (getPosition().x <= 0)
	{
		_velocity.x *= -1;
		setPosition(0, getPosition().y);
	}

	if (getPosition().y >= windowSize.y + 10.0f - rad)
	{
		_velocity.y *= -1;
		//setPosition(getPosition().x, windowSize.y - rad);
		setPosition(10.0f, 300.0f);
	}

	if (getPosition().y <= 0)
	{
		_velocity.y *= -1;
		setPosition(getPosition().x, 0);
	}
}
}