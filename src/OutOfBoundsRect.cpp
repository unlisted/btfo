#include "OutOfBoundsRect.h"

using namespace sf;

namespace BTFO
{

OutOfBoundsRect::OutOfBoundsRect(const Vector2f& vel):
	GameObject(vel)
{
}

OutOfBoundsRect::OutOfBoundsRect(const Vector2f& size, const Vector2f& pos, const Vector2f& vel):
	RectangleShape(size),
	GameObject(vel)
{
	setPosition(pos);
	setFillColor(sf::Color::White);
}

OutOfBoundsRect::~OutOfBoundsRect()
{

}

void OutOfBoundsRect::UpdatePosition(const float& elapsedTime, const sf::RenderWindow& target)
{
}

}
