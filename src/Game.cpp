
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include "Game.h"
#include "Paddle.h"
#include "Projectile.h"
#include "OutOfBoundsRect.h"
#include "Target.h"

using namespace std;
using namespace sf;

namespace BTFO
{

Game::Game():
	_mainWindow(sf::VideoMode(WindowWidth, WindowHeight), "Break The Fuck Out"),
	_state(Running)
{
	_arial.loadFromFile("arial.ttf");
	Reset();
	GameLoop();

}

Game::~Game()
{
}

void Game::Start()
{

}

void Game::Stop()
{
}

void Game::SetupWindow()
{
	_gameObjectMap.clear();
	_targets.clear();

	_gameObjectMap["paddle"] = shared_ptr<GameObject>(new Paddle(Vector2f(40.0f, 10.0f), Vector2f(300.0f, 580.0f)));
	_gameObjectMap["projectile"] = shared_ptr<GameObject>(new Projectile(5.0f, Vector2f(10.0f, 300.0f)));
	_gameObjectMap["bottombound"] = shared_ptr<GameObject>(new OutOfBoundsRect(Vector2f(800.0f, 10.0f), Vector2f(0.0f, 590.0f)));

	int tilesPerRow = (WindowWidth - 20) / TileWidth;
	int tileRows = (WindowHeight / 4) / TileHeight;

	for (auto i = 0; i < tileRows; ++i)
	{
		for (auto j = 0; j < tilesPerRow; ++j)
		{
			_targets.push_back(shared_ptr<GameObject>(new Target(Vector2f(TileWidth, TileHeight), Vector2f(10 + j*(TileWidth+1), 100 + i*(TileHeight+1)))));
		}
	}

}
void Game::Reset()
{
	_lives = 3;
	_points = 0;

	SetupWindow();
	_state = Running;
	
}
void Game::GameLoop()
{
#ifdef LOG_ENABLED
	ofstream ff("out.txt");

    if (!ff.is_open())
    {
        cerr << "Could not open file for writing." << endl;
        return;
    }
#endif

	// stores time to next update 
	chrono::system_clock::time_point nextTime = chrono::system_clock::now();

	// next update delta (50 Hz)
	chrono::milliseconds delta(20);

	while(_mainWindow.isOpen())
	{

		// current time
		chrono::system_clock::time_point currentTime = chrono::system_clock::now();

		// difference between current time and next update
		auto diffTime = chrono::duration_cast<chrono::milliseconds>(currentTime - nextTime).count();

		if (diffTime >= 0)
		{
			// calculate next update time
			nextTime += delta;

#ifdef LOG_ENABLED
			ff << "Updating " << diffTime << "ms late." << endl;
#endif
			// 2. poll for events
			sf::Event evt;
			while (_mainWindow.pollEvent(evt))
			{
				// close event exits application
				switch(evt.type)
				{
				case sf::Event::Closed:
					_state = Exiting;
					_mainWindow.close();
					break;

				case sf::Event::KeyPressed:
					if (evt.key.code == sf::Keyboard::R)
						Reset();

				}
				
			}

			// 3. determine action
			switch (_state)
			{
			case Running:

				// Only hand strafing if game is running
				HandleStrafing();

				_mainWindow.clear();
				CheckForCollisions();
			
				// draw main game objects
				for (auto obj : _gameObjectMap)
				{
					obj.second->UpdatePosition(1, _mainWindow);
					obj.second->Draw(_mainWindow);
				}

				// draw targets
				for (auto obj : _targets)
				{
					obj->Draw(_mainWindow);
				}

				DisplayScore();
				_mainWindow.display();
			
				break;

			case Stopped:
				break;
				//DisplayPoints();
			}
		} else  // go to sleep if current time hasn't reached next update time
		{
			auto sleepTime = nextTime - currentTime;

#ifdef LOG_ENABLED
			ff << "Skipping Update." << endl; 
#endif
			if (sleepTime > chrono::milliseconds(0))
			{
#ifdef LOG_ENABLED
				ff << "Sleeping for " << chrono::duration_cast<chrono::milliseconds>(sleepTime).count() << " ms" << endl;
#endif
				this_thread::sleep_for( sleepTime );
			}
		}
	}
}

void Game::HandleStrafing()
{
	if (Keyboard::isKeyPressed(Keyboard::Left))
	{
		_gameObjectMap["paddle"]->Velocity(Vector2f(-1.5f, 0.0f));
	} else if (Keyboard::isKeyPressed(Keyboard::Right))
	{
		_gameObjectMap["paddle"]->Velocity(Vector2f(1.5f, 0.0f));
	} else 
		_gameObjectMap["paddle"]->Velocity(Vector2f(0.0f, 0.0f));

}

void Game::CheckForCollisions()
{
	// TODO: Fix this sloppyness
	bool collision = _gameObjectMap["paddle"]->CheckForCollision(*_gameObjectMap["projectile"]);
	
	if (collision)
	{
		auto currentVel = _gameObjectMap["projectile"]->Velocity();
		_gameObjectMap["projectile"]->Velocity(Vector2f(currentVel.x, -1.0f * currentVel.y));
	}


	collision = false;
	collision = _gameObjectMap["bottombound"]->CheckForCollision(*_gameObjectMap["projectile"]);
	if (collision)
	{
		try
		{
			dynamic_cast<Shape&>(*_gameObjectMap["projectile"]).setPosition(10.f, 300.0f);

		} catch (const bad_cast& e)
		{
			cerr << e.what() << '\n';
			cerr << "This object is not of type Shape\n";
		}

	
		if (--_lives == 0)
			_state = Stopped;
	}

	vector<std::shared_ptr<GameObject>>::iterator it;
	for(it = _targets.begin(); it != _targets.end(); ++it)
	{
	
		shared_ptr<GameObject> foo(*it);
		bool collision = foo->CheckForCollision(*_gameObjectMap["projectile"]);
		if (collision && foo->Visible())
		{
			foo->Visible(false);
			++_points;
		}
	}

}
 
void Game::DisplayScore()
{


	stringstream ss;
	ss << "Points: " << _points << endl << "Lives: " << _lives;
	string scoreStr = ss.str();
	Text score(scoreStr, _arial);	

	_mainWindow.draw(score);
}
}